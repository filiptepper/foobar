// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var protos_echo_pb = require('../protos/echo_pb.js');

function serialize_echo_Request(arg) {
  if (!(arg instanceof protos_echo_pb.Request)) {
    throw new Error('Expected argument of type echo.Request');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_echo_Request(buffer_arg) {
  return protos_echo_pb.Request.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_echo_Response(arg) {
  if (!(arg instanceof protos_echo_pb.Response)) {
    throw new Error('Expected argument of type echo.Response');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_echo_Response(buffer_arg) {
  return protos_echo_pb.Response.deserializeBinary(new Uint8Array(buffer_arg));
}


var EchoService = exports.EchoService = {
  getEcho: {
    path: '/echo.Echo/GetEcho',
    requestStream: false,
    responseStream: false,
    requestType: protos_echo_pb.Request,
    responseType: protos_echo_pb.Response,
    requestSerialize: serialize_echo_Request,
    requestDeserialize: deserialize_echo_Request,
    responseSerialize: serialize_echo_Response,
    responseDeserialize: deserialize_echo_Response,
  },
};

exports.EchoClient = grpc.makeGenericClientConstructor(EchoService);
