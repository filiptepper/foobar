const spdy = require('spdy');
const fs = require('fs');

var options = {
  key: fs.readFileSync('private-key.pem'),
  cert: fs.readFileSync('certificate.pem')
}

var server = spdy.createServer(options, function(request, response) {
  let body = '';

  request.on('data', chunk => {
    body += chunk.toString();
  });

  request.on('end', () => {
    response.writeHead(200);
    response.end('Hello, ' + body);
  });
}).listen(8443);;
