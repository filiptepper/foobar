var messages = require('./protos/echo_pb');
var services = require('./protos/echo_grpc_pb');

var grpc = require('grpc');

function main() {
  var client = new services.EchoClient('localhost:8443', grpc.credentials.createInsecure());
  var request = new messages.Request();

  request.setMessage('foobar');
  client.getEcho(request, function(err, response) {
    console.log(response.getMessage());
  });
}

main();
