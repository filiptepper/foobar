var messages = require('./protos/echo_pb');
var services = require('./protos/echo_grpc_pb');

var grpc = require('grpc');

function getEcho(call, callback) {
  var reply = new messages.Response();
  reply.setMessage('Hello, ' + call.request.getMessage());
  callback(null, reply);
}

var server = new grpc.Server();
server.addService(services.EchoService, { getEcho: getEcho });
server.bind('0.0.0.0:8443', grpc.ServerCredentials.createInsecure());
server.start();
