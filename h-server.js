const https = require('https');
const fs = require('fs');

const options = {
  key: fs.readFileSync('private-key.pem'),
  cert: fs.readFileSync('certificate.pem')
};

server = https.createServer(options, (request, response) => {
  let body = '';

  request.on('data', chunk => {
    body += chunk.toString();
  });

  request.on('end', () => {
    response.writeHead(200);
    response.end('Hello, ' + body;
  });
}).listen(8443);
