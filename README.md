# certificate

```
openssl req -x509 -newkey rsa:2048 -nodes -sha256 -subj '/CN=localhost' \
  -keyout private-key.pem -out certificate.pem
```

# Benchmarks

Very unscientific benchmarks.

## HTTP

```
$ ab -n 10000 -c 1 -k -p POST -T application/octet-stream https://localhost:8443/
This is ApacheBench, Version 2.3 <$Revision: 1826891 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 1000 requests
Completed 2000 requests
Completed 3000 requests
Completed 4000 requests
Completed 5000 requests
Completed 6000 requests
Completed 7000 requests
Completed 8000 requests
Completed 9000 requests
Completed 10000 requests
Finished 10000 requests


Server Software:        
Server Hostname:        localhost
Server Port:            8443
SSL/TLS Protocol:       TLSv1.2,ECDHE-RSA-AES128-GCM-SHA256,2048,128
TLS Server Name:        localhost

Document Path:          /
Document Length:        15 bytes

Concurrency Level:      1
Time taken for tests:   81.708 seconds
Complete requests:      10000
Failed requests:        0
Keep-Alive requests:    0
Total transferred:      900000 bytes
Total body sent:        1730000
HTML transferred:       150000 bytes
Requests per second:    122.39 [#/sec] (mean)
Time per request:       8.171 [ms] (mean)
Time per request:       8.171 [ms] (mean, across all concurrent requests)
Transfer rate:          10.76 [Kbytes/sec] received
                        20.68 kb/s sent
                        31.43 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        7    8   0.6      8      23
Processing:     0    0   0.1      0       8
Waiting:        0    0   0.1      0       7
Total:          7    8   0.6      8      31

Percentage of the requests served within a certain time (ms)
  50%      8
  66%      8
  75%      8
  80%      8
  90%      8
  95%      9
  98%     10
  99%     11
 100%     31 (longest request)
```

## HTTP/2

```
$ h2load https://localhost:8443/ -n10000 -c1 -m1 
starting benchmark...
spawning thread #0: 1 total client(s). 10000 total requests
TLS Protocol: TLSv1.2
Cipher: ECDHE-RSA-AES128-GCM-SHA256
Server Temp Key: ECDH P-256 256 bits
Application protocol: h2
progress: 10% done
progress: 20% done
progress: 30% done
progress: 40% done
progress: 50% done
progress: 60% done
progress: 70% done
progress: 80% done
progress: 90% done
progress: 100% done

finished in 4.60s, 2175.12 req/s, 76.48KB/s
requests: 10000 total, 10000 started, 10000 done, 10000 succeeded, 0 failed, 0 errored, 0 timeout
status codes: 10000 2xx, 0 3xx, 0 4xx, 0 5xx
traffic: 351.61KB (360049) total, 9.77KB (10000) headers (space savings 90.00%), 78.13KB (80000) data
                     min         max         mean         sd        +/- sd
time for request:      223us     10.72ms       451us       482us    95.21%
time for connect:     8.29ms      8.29ms      8.29ms         0us   100.00%
time to 1st byte:    11.33ms     11.33ms     11.33ms         0us   100.00%
req/s           :    2175.52     2175.52     2175.52        0.00   100.00%
```

```
$ h2load https://localhost:8443/ -n10000 -c1 -m5
starting benchmark...
spawning thread #0: 1 total client(s). 10000 total requests
TLS Protocol: TLSv1.2
Cipher: ECDHE-RSA-AES128-GCM-SHA256
Server Temp Key: ECDH P-256 256 bits
Application protocol: h2
progress: 10% done
progress: 20% done
progress: 30% done
progress: 40% done
progress: 50% done
progress: 60% done
progress: 70% done
progress: 80% done
progress: 90% done
progress: 100% done

finished in 2.95s, 3387.74 req/s, 119.12KB/s
requests: 10000 total, 10000 started, 10000 done, 10000 succeeded, 0 failed, 0 errored, 0 timeout
status codes: 10000 2xx, 0 3xx, 0 4xx, 0 5xx
traffic: 351.61KB (360049) total, 9.77KB (10000) headers (space savings 90.00%), 78.13KB (80000) data
                     min         max         mean         sd        +/- sd
time for request:      595us     11.97ms      1.43ms      1.12ms    90.94%
time for connect:     9.68ms      9.68ms      9.68ms         0us   100.00%
time to 1st byte:    20.63ms     20.63ms     20.63ms         0us   100.00%
req/s           :    3387.88     3387.88     3387.88        0.00   100.00%
```

```
$ h2load https://localhost:8443/ -n10000 -c1 -m10
starting benchmark...
spawning thread #0: 1 total client(s). 10000 total requests
TLS Protocol: TLSv1.2
Cipher: ECDHE-RSA-AES128-GCM-SHA256
Server Temp Key: ECDH P-256 256 bits
Application protocol: h2
progress: 10% done
progress: 20% done
progress: 30% done
progress: 40% done
progress: 50% done
progress: 60% done
progress: 70% done
progress: 80% done
progress: 90% done
progress: 100% done

finished in 2.48s, 4025.56 req/s, 141.54KB/s
requests: 10000 total, 10000 started, 10000 done, 10000 succeeded, 0 failed, 0 errored, 0 timeout
status codes: 10000 2xx, 0 3xx, 0 4xx, 0 5xx
traffic: 351.61KB (360049) total, 9.77KB (10000) headers (space savings 90.00%), 78.13KB (80000) data
                     min         max         mean         sd        +/- sd
time for request:     1.00ms     29.95ms      2.40ms      1.80ms    89.75%
time for connect:     6.32ms      6.32ms      6.32ms         0us   100.00%
time to 1st byte:    36.15ms     36.15ms     36.15ms         0us   100.00%
req/s           :    4026.01     4026.01     4026.01        0.00   100.00%
```

```
$ h2load https://localhost:8443/ -n10000 -c1 -m50
starting benchmark...
spawning thread #0: 1 total client(s). 10000 total requests
TLS Protocol: TLSv1.2
Cipher: ECDHE-RSA-AES128-GCM-SHA256
Server Temp Key: ECDH P-256 256 bits
Application protocol: h2
progress: 10% done
progress: 20% done
progress: 30% done
progress: 40% done
progress: 50% done
progress: 60% done
progress: 70% done
progress: 80% done
progress: 90% done
progress: 100% done

finished in 2.34s, 4280.78 req/s, 150.52KB/s
requests: 10000 total, 10000 started, 10000 done, 10000 succeeded, 0 failed, 0 errored, 0 timeout
status codes: 10000 2xx, 0 3xx, 0 4xx, 0 5xx
traffic: 351.61KB (360049) total, 9.77KB (10000) headers (space savings 90.00%), 78.13KB (80000) data
                     min         max         mean         sd        +/- sd
time for request:     4.59ms     58.83ms     11.21ms      6.78ms    86.75%
time for connect:     5.65ms      5.65ms      5.65ms         0us   100.00%
time to 1st byte:    63.35ms     63.35ms     63.35ms         0us   100.00%
req/s           :    4281.02     4281.02     4281.02        0.00   100.00%
```

## Socket

Using JMeter: 9157.509157509157 req/s.

## GRPC

```
$ ./ghz -proto protos/echo.proto -call 'echo.Echo.GetEcho' -insecure -c 1 -n 10000 -d '{"message":"foobar"}' -L 10  localhost:8443

Summary:
  Count:  10000
  Total:  2921.80 ms
  Slowest:  5.63 ms
  Fastest:  0.15 ms
  Average:  0.23 ms
  Requests/sec: 3422.55

Response time histogram:
  0.150 [1] |
  0.698 [9967]  |∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
  1.246 [28]  |
  1.793 [2] |
  2.341 [1] |
  2.889 [0] |
  3.436 [0] |
  3.984 [0] |
  4.532 [0] |
  5.079 [0] |
  5.627 [1] |

Latency distribution:
  10% in 0.18 ms
  25% in 0.20 ms
  50% in 0.22 ms
  75% in 0.25 ms
  90% in 0.29 ms
  95% in 0.34 ms
  99% in 0.51 ms
Status code distribution:
  [OK]  10000 responses
```
